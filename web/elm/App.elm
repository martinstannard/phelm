module App where

import String exposing (..)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick)
import StartApp.Simple as StartApp


main =
  StartApp.start { model = model, view = view, update = update }

-- MODEL

type alias Card =
  { rank : String
  , suit : String
  }

type alias Hand =
  List Card

type alias Deck =
  List Card


ranks : List String
ranks = ["2", "3", "4", "5", "6", "7", "8", "9", "10", "jack", "queen", "king", "ace"]

suitor : String -> String -> Card
suitor rank suit =
  makeCard rank suit 


generateDeck : () -> Deck
generateDeck () = 
  List.map (\n -> (suitor n "Hearts")) ranks ++
  List.map (\n -> (suitor n "Clubs")) ranks ++
  List.map (\n -> (suitor n "Diamonds")) ranks ++
  List.map (\n -> (suitor n "Spades")) ranks


scoreCard : Card -> Int
scoreCard card =
  case card.rank of
    "ace" -> 10
    "jack" -> 10
    "queen" -> 10
    _ -> 2
    --_ -> Result.withDefault 10 (String.toInt card.rank)


scoreHand : Hand -> Int
scoreHand hand =
  List.foldl (+) 0 (List.map scoreCard hand)


makeCard : String -> String -> Card
makeCard r s =
  { rank = r, suit = s}


cardName : Card -> String
cardName card =
  "/images/cards/" ++ card.rank ++ "_of_" ++ (String.toLower card.suit) ++ ".png"


type alias Model =
  { deck : Deck
  , hand : Hand
  }

model = initialModel

initialModel : Model
initialModel = 
  { deck = generateDeck() 
  , hand = []
  }

-- UPDATE

type Action = Hit | Stand | New


update : Action -> Model -> Model
update action model =
  case action of
    Hit ->
      { model |
          hand = (List.take 1 model.deck) ++ model.hand,
          deck = List.drop 1 model.deck
      }

    Stand ->
      model

    New ->
      { model |
          hand = [],
          deck = generateDeck()
      }

-- VIEW
hitStyle: Int -> List (String, String)
hitStyle score =
  if score > 21 then
     [("display", "none")]
  else 
     [("display", "inline")]

cardView : Int -> Card -> Html
cardView size card =
  span []
    [
      img [ src(cardName(card)), width size] [ ]
    ]

handCard = cardView 80
deckCard = cardView 25

view : Signal.Address Action -> Model -> Html
view address model =
  div []
    [ div [] (List.map deckCard model.deck)
    , handView model.hand
    , button [ onClick address Hit, style(hitStyle(scoreHand model.hand)) ] [ text "Hit" ]
    , button [ onClick address Stand ] [ text "Stand" ]
    , button [ onClick address New ] [ text "New" ]
    , div [] [text (toString (scoreHand model.hand))]
    ]

handView : Hand -> Html
handView hand =
  div []
    (List.map handCard hand)

