defmodule Bj.PageController do
  use Bj.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
