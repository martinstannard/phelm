ExUnit.start

Mix.Task.run "ecto.create", ~w(-r Bj.Repo --quiet)
Mix.Task.run "ecto.migrate", ~w(-r Bj.Repo --quiet)
Ecto.Adapters.SQL.begin_test_transaction(Bj.Repo)

